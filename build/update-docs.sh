#!/bin/bash
# This script updates all of the logs in the root directory, and rebuilds the
# manual, in preparation for a release.

# Make sure we're being run in the right directory
if [[ ! -f AUTHORS || ! -f README.md ]]; then
  echo 'You must run this script from the root of the repository.'
  exit 1
fi

# Make sure we were given the path to the wiki as the first argument.
WIKI_PATH="$1"
if [[ ! -d $WIKI_PATH || ! -f $WIKI_PATH/Home.md ]]; then
  echo 'You must pass this script the wiki directory as its first parameter.'
  echo "$0 <path-to-wiki> <path-to-hhp2cached>"
  exit 1
fi

# Make sure we were given the path to hhp2cached
HHP2CACHED="$2"
if [[ ! -e $HHP2CACHED ]]; then
  echo 'You must pass this script the hhp2cached binary as its second parameter.'
  echo "$0 <path-to-wiki> <path-to-hhp2cached>"
  exit 1
fi

# This should point to the last commit in the year *before* the current year.
LAST_YEAR_COMMIT=e09b2c8f1926ee9f5389336bebc6e7072e647afb

# Update ChangeLog
git log --no-color --no-merges --date=short --no-color \
  --pretty="format:%cd  %cn <%ce>%n%n  * %s%n" \
  $LAST_YEAR_COMMIT.. > ChangeLog

# Update NEWS
pandoc -f markdown -t plain -o NEWS $WIKI_PATH/Release-Notes.md

# Remove the temporary docs directory if it exists
rm -rf temp-docs-dir

# Copy images across
mkdir -p temp-docs-dir/images
cp $WIKI_PATH/images/* temp-docs-dir/images/

# Copy templates
cp doc/manual/tools/oyun-manual.tex temp-docs-dir/
cp doc/manual/tools/oyun-contents.hhc temp-docs-dir/
cp doc/manual/tools/oyun-index.hhk temp-docs-dir/
cp doc/manual/tools/oyun.hhp temp-docs-dir/

# Convert markdown to HTML for wxWidgets help files
for md in $WIKI_PATH/*.md; do
  html_out="temp-docs-dir/`basename $md .md`.html"

  pandoc -f markdown -t html --template=doc/manual/tools/template.html \
    -o $html_out $md

  # Do some cleanups
  tidy -m --quiet 1 -b -u -wrap -omit -utf8 --output-html 1 --tidy-mark 0 \
    --show-errors 0 --show-warnings 0 $html_out
  sed -i'' -e "s@<DIV id=.*>@@g" $html_out
  sed -i'' -e "s@</DIV>@@g" $html_out
done

# Convert markdown to TeX for PDF manual
for md in $WIKI_PATH/*.md; do
  tex_out="temp-docs-dir/`basename $md .md`.tex"
  pandoc -f markdown -t latex -o $tex_out $md
done

# Build the PDF manual
cd temp-docs-dir
pdflatex oyun-manual -interaction batchmode >/dev/null 2>&1
pdflatex oyun-manual -interaction batchmode >/dev/null 2>&1
pdflatex oyun-manual -interaction batchmode >/dev/null 2>&1

# Build the cached HHP
../$HHP2CACHED oyun.hhp

# Pack everything into an htb
zip -q "oyun.htb" *.html images/* oyun.hhp.cached oyun.hhp oyun-index.hhk \
  oyun-contents.hhc

cd ..

# Put the files where they go
cp temp-docs-dir/oyun-manual.pdf doc/manual
cp temp-docs-dir/oyun.htb doc/manual

# Clean up
rm -rf temp-docs-dir
