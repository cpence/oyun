# We have to get creative, because wxWidgets installs to a really weird place
# with vcpkg, and it's not picked up by the usual wxWidgets finders

if (MSVC)
  # Use vcpkg-installed wxWidgets 3.1
  add_definitions (-D_UNICODE -DUNICODE -D__WXMSW__)
  include_directories (${_VCPKG_INSTALLED_DIR}/x64-windows-static/include)

  # We're statically linking whenever we use wxWidgets on Windows, make sure
  # that the CRT does the same
  set (CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT")
  set (CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MTd")

  if (CMAKE_BUILD_TYPE MATCHES "Debug")
    add_definitions (-D_DEBUG)
    include_directories (${_VCPKG_INSTALLED_DIR}/x64-windows-static/debug/lib/mswud)
    set (wxWidgets_LIB_DIR ${_VCPKG_INSTALLED_DIR}/x64-windows-static/debug/lib)
    set (wxWidgets_LIBRARIES
      ${wxWidgets_LIB_DIR}/wxmsw31ud_html.lib
      ${wxWidgets_LIB_DIR}/wxmsw31ud_adv.lib
      ${wxWidgets_LIB_DIR}/wxmsw31ud_core.lib
      ${wxWidgets_LIB_DIR}/wxbase31ud.lib
      ${wxWidgets_LIB_DIR}/jpeg.lib
      ${wxWidgets_LIB_DIR}/libpng16d.lib
      ${wxWidgets_LIB_DIR}/tiffd.lib
      ${wxWidgets_LIB_DIR}/expat.lib
      ${wxWidgets_LIB_DIR}/lzma.lib
      ${wxWidgets_LIB_DIR}/zlibd.lib
      winmm comctl32 oleacc rpcrt4 shlwapi version wsock32
    )
  elseif (CMAKE_BUILD_TYPE MATCHES "Release")
    include_directories (${_VCPKG_INSTALLED_DIR}/x64-windows-static/lib/mswu)
    set (wxWidgets_LIB_DIR ${_VCPKG_INSTALLED_DIR}/x64-windows-static/lib)
    set (wxWidgets_LIBRARIES
      ${wxWidgets_LIB_DIR}/wxmsw31u_html.lib
      ${wxWidgets_LIB_DIR}/wxmsw31u_adv.lib
      ${wxWidgets_LIB_DIR}/wxmsw31u_core.lib
      ${wxWidgets_LIB_DIR}/wxbase31u.lib
      ${wxWidgets_LIB_DIR}/jpeg.lib
      ${wxWidgets_LIB_DIR}/libpng16.lib
      ${wxWidgets_LIB_DIR}/tiff.lib
      ${wxWidgets_LIB_DIR}/expat.lib
      ${wxWidgets_LIB_DIR}/lzma.lib
      ${wxWidgets_LIB_DIR}/zlib.lib
      winmm comctl32 oleacc rpcrt4 shlwapi version wsock32
    )
  endif ()
else ()
  # Do what works everywhere else
  find_package (wxWidgets 3 REQUIRED base core adv html)
  include (${wxWidgets_USE_FILE})
endif ()
