/* NOLINT(legal/copyright)
 * This file has been released into the public domain.
 */

#ifndef FAILURE_H_
#define FAILURE_H_

class Failure {
 public:
  Failure(const wxString& theTestName, const wxString& theFileName,
          int theLineNumber, const wxString& theCondition);
  Failure(const wxString& theTestName, const wxString& theFileName,
          int theLineNumber, const wxString& expected, const wxString& actual);

  wxString message;
  wxString testName;
  wxString fileName;
  int lineNumber;
};

#endif  // FAILURE_H_
