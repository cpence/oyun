/* NOLINT(legal/copyright)
 * This file has been released into the public domain.
 */

#ifndef TESTREGISTRY_H_
#define TESTREGISTRY_H_

#include <map>

class Test;
class TestResult;

class TestRegistry {
 public:
  static void addTest(Test *test);
  static bool runAllTests(TestResult *result);

 private:
  static TestRegistry& instance();
  void add(Test *test);
  bool run(TestResult *result);

  std::map<wxString, Test *> tests;
};

#endif  // TESTREGISTRY_H_
