/* NOLINT(legal/copyright)
 * This file has been released into the public domain.
 */

#ifndef TESTHARNESS_H_
#define TESTHARNESS_H_

#include "Test.h"
#include "TestResult.h"
#include "Failure.h"
#include "TestRegistry.h"

#endif  // TESTHARNESS_H_
