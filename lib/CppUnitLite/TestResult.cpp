/* NOLINT(legal/copyright)
 * This file has been released into the public domain.
 */

#include <wx/wxprec.h>
#ifdef __BORLANDC__
#  pragma hdrstop
#endif

#ifndef WX_PRECOMP
#  include <wx/wx.h>
#endif

#include "TestResult.h"
#include "Failure.h"

TestResult::TestResult() : testCount(0), checkCount(0), failureCount(0) { }

void TestResult::testsStarted() {
  wxPrintf("Beginning unit tests for Oyun...\n");
}

void TestResult::runTest() {
  testCount++;
}

void TestResult::addSuccess() {
  checkCount++;
}

void TestResult::addFailure(const Failure& failure) {
  wxPrintf("\nFailure: \"%s\" line %d in %s", failure.message,
           failure.lineNumber, failure.fileName);

  failureCount++;
  checkCount++;
}

void TestResult::testsEnded() {
  if (failureCount > 0)
    wxPrintf("\n%ld tests run [%ld checks]: there were %ld failures!\n",
             testCount, checkCount, failureCount);
  else
    wxPrintf("\n%ld tests run [%ld checks]: all tests passed!\n",
             testCount, checkCount);
}
