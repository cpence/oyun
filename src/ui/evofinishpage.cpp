/*
    Copyright (C) 2004-2018 by Charles H. Pence
    charles@charlespence.net

    This file is part of Oyun.

    Oyun is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Oyun is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Oyun.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <wx/wxprec.h>
#ifdef __BORLANDC__
#  pragma hdrstop
#endif
#ifndef WX_PRECOMP
#  include <wx/wx.h>
#endif

#include <wx/file.h>
#include <wx/textfile.h>
#include <wx/filename.h>
#include <wx/wfstream.h>

#include "ui/evofinishpage.h"
#include "ui/oyunapp.h"
#include "ui/oyunwizard.h"
#include "ui/evopage.h"
#include "tourney/evotournament.h"

enum {
  ID_SAVE_IMAGE = wxID_HIGHEST,
  ID_SAVE_SVG,
  ID_SAVE_CSV
};

IMPLEMENT_CLASS(EvoFinishPage, FinishPage)

BEGIN_EVENT_TABLE(EvoFinishPage, FinishPage)
  EVT_WIZARD_HELP(wxID_ANY, EvoFinishPage::OnHelp)
  EVT_BUTTON(ID_SAVE_IMAGE, EvoFinishPage::OnSaveImage)
  EVT_BUTTON(ID_SAVE_SVG, EvoFinishPage::OnSaveSVG)
  EVT_BUTTON(ID_SAVE_CSV, EvoFinishPage::OnSaveCSV)

  EVT_NOTIFY(wxEVT_DATA_UPDATE, wxID_ANY, EvoFinishPage::OnDataUpdate)
END_EVENT_TABLE()

EvoFinishPage::EvoFinishPage(OyunWizard *parent, EvoPage *prev) :
  FinishPage(parent, reinterpret_cast<wxWizardPage *>(prev)) {
  previous = prev;

  AddButton(ID_SAVE_IMAGE, _("Save &Image..."),
            _("Save an image of the graph of the tournament results.\n\n"
              "You can save the graph displayed on the previous page in\n"
              "any one of several image formats (like JPG, PNG, BMP, and\n"
              "so on)."));
  AddButton(ID_SAVE_SVG, _("Save &SVG..."),
            _("Save an SVG image of the graph of the tournament results.\n\n"
              "This is a scalable image, with better quality than the above,\n"
              "but can only be opened by some programs (such as Adobe \n"
              "Illustrator or Inkscape)."));
  AddButton(ID_SAVE_CSV, _("Save CS&V..."),
            _("Save a spreadsheet of the detailed tournament data.\n\n"
              "This is a spreadsheet containing the frequency of each player\n"
              "at each generation in the evolutionary tournament."));
}

void EvoFinishPage::OnDataUpdate(wxNotifyEvent & WXUNUSED(event)) {
  // Set our focus and defaults if we're visible
  if (IsShownOnScreen()) {
    if (dataSaved) {
      // Set the focus to the Next button
      wxWindow *win = wxWindow::FindWindowById(wxID_FORWARD, GetParent());
      wxButton *nextButton = wxDynamicCast(win, wxButton);

      if (nextButton) {
        nextButton->SetDefault();
        nextButton->SetFocus();
      }
    } else {
      // Set the focus to the CSV button
      wxWindow *win = wxWindow::FindWindowById(ID_SAVE_IMAGE, this);
      wxButton *csvButton = wxDynamicCast(win, wxButton);

      if (csvButton) {
        csvButton->SetDefault();
        csvButton->SetFocus();
      }
    }
  }
}

void EvoFinishPage::OnHelp(wxWizardEvent & WXUNUSED(event)) {
  wxGetApp().ShowHelp(this, "Evolutionary-Tournament-Finished-Page.html");
}

void EvoFinishPage::OnSaveImage(wxCommandEvent & WXUNUSED(event)) {
  // Get a filename from the user
  static const wxString filter(_("JPEG image (*.jpg)|*.jpg|"
                                 "PNG image (*.png)|*.png|"
                                 "BMP image (*.bmp)|*.bmp"));
  wxString str;

  str = wxFileSelector(_("Select where to save the image file"), wxEmptyString,
                       _("graph.jpg"), ".jpg", filter,
                       wxFD_SAVE | wxFD_OVERWRITE_PROMPT, this);
  if (str.IsEmpty())
    return;

  // Figure out what the type of the file is
  wxFileName filename(str);
  wxString extension = filename.GetExt();
  wxString mimeType;

  if (extension == "jpg") {
    mimeType = "image/jpeg";
  } else if (extension == "png") {
    mimeType = "image/png";
  } else if (extension == "bmp") {
    mimeType = "image/x-bmp";
  } else {
    filename.SetExt("jpg");
    mimeType = "image/jpeg";
  }

  // Save the image file
  wxFileOutputStream fileStream(filename.GetFullPath());
  if (fileStream.IsOk())
    previous->imageGraph.SaveFile(fileStream, mimeType);
  // else
  // FIXME: show error box

  dataSaved = true;
}

void EvoFinishPage::OnSaveSVG(wxCommandEvent & WXUNUSED(event)) {
  // Get a filename from the user
  static const wxString filter(_("SVG image (*.svg)|*.svg"));
  wxString str;

  str = wxFileSelector(_("Select where to save the SVG file"), wxEmptyString,
                       _("graph.svg"), ".svg", filter,
                       wxFD_SAVE | wxFD_OVERWRITE_PROMPT, this);
  if (str.IsEmpty())
    return;

  // Figure out what the type of the file is
  wxFileName filename(str);
  wxString extension = filename.GetExt();

  if (extension != "svg")
    filename.SetExt("svg");

  // Save the SVG file
  wxFile file(filename.GetFullPath(), wxFile::write);
  if (!file.IsOpened())
    return;

  file.Write(previous->svgGraph);
  file.Flush();
  file.Close();

  dataSaved = true;
}

void EvoFinishPage::OnSaveCSV(wxCommandEvent & WXUNUSED(event)) {
  // Get a filename from the user
  static const wxString filter(_("CSV spreadsheet (*.csv)|*.csv"));
  wxString str;

  str = wxFileSelector(_("Select where to save the CSV file"), wxEmptyString,
                       _("tournament.csv"), ".csv", filter,
                       wxFD_SAVE | wxFD_OVERWRITE_PROMPT, this);
  if (str.IsEmpty())
    return;

  // Figure out what the type of the file is
  wxFileName filename(str);
  wxString extension = filename.GetExt();

  if (extension != "csv")
    filename.SetExt("csv");

  // Save the CSV file
  wxTextFile file(filename.GetFullPath());

  if (file.Exists()) {
    file.Open();
    file.Clear();
  } else {
    file.Create();
  }

  // Generate the lines of the CSV file
  size_t numPlayers = previous->evoTourney->players.GetCount();
  size_t numGenerations = previous->evoTourney->data.GetCount();
  wxArrayString lines;
  lines.SetCount(numPlayers);

  for (size_t p = 0 ; p < numPlayers ; p++) {
    Player *player = previous->evoTourney->players[p];
    int id = player->GetID();

    // Start by writing out the player name and author
    wxString playerData;
    playerData = player->GetPlayerName() + "," +
                 player->GetPlayerAuthor() + ",";

    lines[p] = playerData;

    for (size_t gen = 0 ; gen < numGenerations ; gen++) {
      // Get this generation's worth of data
      GenerationWeights &w = previous->evoTourney->data[gen];

      // Get the value for this player at this generation
      wxString val;
      val.Printf("%f", w[id]);

      // Append a comma if we need to
      if (gen != numGenerations - 1)
        val += ",";

      // Add the value to the line
      lines[p] += val;
    }
  }

  // Write the file header
  wxString header = _("Player Name") + "," +
                    _("Player Author") + "," +
                    _("Initial Fraction") + ",";
  for (size_t gen = 1 ; gen < numGenerations ; gen++) {
    header += wxString::Format(_("Generation %d"), gen);
    if (gen != numGenerations - 1)
      header += ",";
  }
  file.AddLine(header);

  // Write the data
  for (size_t p = 0 ; p < numPlayers ; p++)
    file.AddLine(lines[p]);

  file.Write();
  file.Close();

  dataSaved = true;
}
