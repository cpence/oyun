/*
    Copyright (C) 2004-2018 by Charles H. Pence
    charles@charlespence.net

    This file is part of Oyun.

    Oyun is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Oyun is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Oyun.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef BUILD_TESTS

#include <wx/wxprec.h>
#ifdef __BORLANDC__
#  pragma hdrstop
#endif

#ifndef WX_PRECOMP
#  include <wx/wx.h>
#endif

#include <TestHarness.h>

#include "common/filesystem.h"

class OyunTestApp : public wxAppConsole {
 public:
  virtual bool OnInit();
  virtual int OnRun();
};

wxIMPLEMENT_APP(OyunTestApp);

int test_result;

bool OyunTestApp::OnInit() {
  wxString path;
  FS::GetRealPath(argv[0], &path);
  wxPrintf("Running tests for %s\n", path);

  TestResult result;

  // Return the success status in the exit code
  if (TestRegistry::runAllTests(&result))
    test_result = EXIT_SUCCESS;
  else
    test_result = EXIT_FAILURE;

  return true;
}

int OyunTestApp::OnRun() {
  return test_result;
}

#endif

