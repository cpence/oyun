/*
    Copyright (C) 2004-2018 by Charles H. Pence
    charles@charlespence.net

    This file is part of Oyun.

    Oyun is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Oyun is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Oyun.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMON_ERROR_H_
#define COMMON_ERROR_H_


/**
    \namespace Error
    \brief Namespace containing error handling code
*/
namespace Error {

/**
    \brief Set current error string
    \ingroup common

    \param str Current error string, override old error string
*/
void Set(const wxString &str);


/**
    \brief Get current error string
    \ingroup common

    \returns Current error string
*/
const wxString Get(void);

}  // namespace Error

#endif  // COMMON_ERROR_H_
