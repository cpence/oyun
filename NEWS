Here you’ll find what’s been added or changed in various versions of
Oyun.



WHAT’S NEW


This is the first beta release of Oyun 2.0. Test it thoroughly and
report any bugs you happen to find!



CHANGE LOG


-   _Version 2.0.?? [???]:_

-   _Version 2.0.b6 [June 1, 2018]:_
    -   Fix a few small bugs in RTF output, require wxWidgets 3.0.
    -   Build our releases on Travis and Appveyor.
    -   Check the manuals into the git repository.
-   _Version 2.0.b5 [December 8, 2011]:_ Fix small bug regarding extra
    spaces in FSM player files.

-   _Version 2.0.b4 [June 16, 2010]:_ Project renamed to “Oyun”.

-   _Version 2.0.b2 [October 21, 2010]:_ First beta release of Oyun 2.0.
    New features include:
    -   Entirely reworked “wizard” interface
    -   A new graph-drawing system for the evolutionary tournaments
    -   Revamped support for saving of tournament data
    -   Full tested portability to Linux, Windows, and OS X
    -   Backed by a detailed suite of unit tests
-   _Version 2.0.b1:_ Internal release for testing.

-   _Version 1.0:_ Initial release, which featured a rather clumsy
    tabbed notebook interface.



LICENSE


Oyun itself is licensed under the GNU GPL. The license is available in
the root of the source distribution, in a file called COPYING. In
legalese:

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.

As Oyun uses wxWidgets, which uses libjpeg from the Independent JPEG
Group, we are obligated to say here that “this software is based in part
on the work of the Independent JPEG Group.”

All of Oyun’s documentation is subject to the Creative Commons
Attribution-Share Alike 3.0 United States license, and is copyright ©
2009-2018 Charles H. Pence.
